$(document).ready(function(){
  var $name = $('.name')
  var $price = $('.price')
  var $submit = $('.button')
  var $tableBody = $('.table-body')
  var $total = $('h3')
  var count = 0, total = 0

  $submit.click(function(){
    count += 1
    total += parseFloat($price.val())

    $total.text('Total $' + total.toFixed(2))

    $tableBody.append(
      '<tr><td class="text-center">' + count + '</td><td>' + $name.val() +
      "</td><td class='data' price-v='" + $price.val() + "'>" + '$' + $price.val() +
      "</td><td class='text-center'>" +
      '<i class="fa fa-trash-o remove" aria-hidden="true"></i></tr>')
  })

  $(document).on('click', '.remove', function(){
    var temp = parseFloat($(this).closest('tr').find('.data').attr('price-v'))
    total = total - temp

    $(this).closest('tr').remove()
    $total.text('Total $' + total.toFixed(2))
  })
})
